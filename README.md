An image to download the contents of a Box folder you own into a volume on OKD.

Make a Box application and get a short-lived developer token with read _and_ write access:
https://duke.app.box.com/developers/console

```
mv okd.default/ okd/
--- edit yaml, adding your namespace ---
mv .env.default .env
--- edit env variables with your box application details and folder id ---
oc create secret generic quick-box-api-env --from-env-file=.env
oc create -f okd/persistentVolumeClaimBox.yml
oc create -f okd/deploymentConfigBox.yml
--- do stuff in the OKD console Terminal ---
oc delete -f okd/deploymentConfigBox.yml
```