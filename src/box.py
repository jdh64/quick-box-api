import os
from boxsdk import Client, OAuth2


def download_folder_items():

    auth = OAuth2(
        client_id=os.environ['BOX_CLIENT_ID'],
        client_secret=os.environ['BOX_CLIENT_SECRET'],
        access_token=os.environ['BOX_DEVELOPER_TOKEN'],
    )

    client = Client(auth)

    folder = client.folder(folder_id=os.environ['BOX_FOLDER_ID'])

    items = folder.get_items()
    for item in items:
        print('{0} {1} is named "{2}"'.format(item.type.capitalize(), item.id, item.name))
        output_file = open('/srv/data/' + str(item.name), 'wb')
        item.download_to(output_file)
        
    return True

