import typer
from box import download_folder_items


app = typer.Typer()


@app.command()
def download():
    download_folder_items()


if __name__ == "__main__":
    app()
