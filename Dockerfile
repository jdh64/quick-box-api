FROM continuumio/miniconda3

WORKDIR /srv/src
COPY src /srv/src

RUN apt-get update \
    && apt-get -y install gnupg wget unzip curl \
    && apt-get update && apt-get install -y unixodbc-dev gcc g++ \
    && apt-get install -y postgresql-client

RUN conda install -n base -c anaconda --file requirements.txt \
    && conda clean --all --yes \
    && pip3 install "typer[all]" \
    && pip3 install "boxsdk"

CMD ["tail", "-f", "/dev/null"]
